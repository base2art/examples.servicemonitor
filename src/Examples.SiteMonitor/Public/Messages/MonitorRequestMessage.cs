﻿namespace Examples.SiteMonitor.Public.Messages
{
    using System;
    using System.Collections.Generic;

    public class MonitorRequestMessage
    {
        public Guid Id { get; set; }
        
        public string Url { get; set; }

        public string Method { get; set; }
        
        public Dictionary<string, object> Body { get; set; }
    }
}
