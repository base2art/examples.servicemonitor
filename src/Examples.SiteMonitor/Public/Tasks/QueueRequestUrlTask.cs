﻿namespace Examples.SiteMonitor.Public.Tasks
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.GatedQueue;
    using Base2art.Tasks;
    using Examples.SiteMonitor.Data;
    using Examples.SiteMonitor.Public.Messages;
    
    public class QueueRequestUrlTask
    {
        private readonly IDataStore store;

        private readonly ITypedMessageQueue queue;
        
        public QueueRequestUrlTask(IDataStoreFactory store, string connectionName, ITypedMessageQueue queue)
        {
            this.queue = queue;
            this.store = store.Create(connectionName);
        }
        
        public async Task ExecuteAsync()
        {
            var monitors = await this.store.Select<monitor_v1>()
                .Where(rs => rs.Field(x => x.is_active, true, (x, y) => x == y))
                .Execute();
            
            
            foreach (var monitor in monitors)
            {
                Func<monitor_v1, DateTime> when = x => x == null || !x.when.HasValue
                    ? DateTime.MinValue
                    : x.when.Value;
                
                var lastRunTime = when(monitor);
                
                if (lastRunTime.Add(monitor.interval) < DateTime.UtcNow)
                {
                    var request = new MonitorRequestMessage {
                        Id = monitor.id,
                        Body = monitor.body,
                        Method = monitor.method,
                        Url = monitor.url,
                    };
                    
                    await queue.OfferWithApproval(
                        "monitor_request_message:" + request.Id, request, "system", 0, TimeSpan.FromSeconds(20));
                    
                    await this.store.Update<monitor_v1>()
                        .Set(rs => rs.Field(x => x.when, DateTime.UtcNow))
                        .Where(rs => rs.Field(x => x.id, monitor.id, (x, y) => x == y))
                        .Execute();
                }
            }
        }
    }
}
