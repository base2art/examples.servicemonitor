﻿namespace Examples.SiteMonitor.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public interface monitor_v1
    {
        Guid id { get; }
        
        [MaxLength(850)]
        string url { get; }

        [MaxLength(8)]
        string method { get; }
        
        Dictionary<string, object> body { get; }
        
        TimeSpan interval { get; }
        
        bool is_active { get; }
        
        DateTime? when { get; }
    }
}
