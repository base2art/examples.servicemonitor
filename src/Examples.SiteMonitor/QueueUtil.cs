﻿namespace Examples.SiteMonitor
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.GatedQueue;
    
    public static class QueueUtil
    {
        public static async Task PollProcessUpdate<T>(this ITypedMessageQueue queue, Func<T, Task> task)
        {
            var item = await queue.Poll<T>();
            if (item == null)
            {
                return;
            }
            Exception ex = null;
            try
            {
                await task(item.WorkItemData);
                await queue.MarkSuccessful(item.Id, "Refreshed...");
            }
            catch (Exception exi)
            {
                ex = exi;
            }
            if (ex != null)
            {
                await queue.MarkFailed(item.Id, ex);
                throw new TargetInvocationException(ex);
            }
        }
    }
}


