﻿namespace Examples.SiteMonitor.Public.Tasks
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.GatedQueue;
    using Examples.SiteMonitor.Data;
    using Examples.SiteMonitor.Public.Messages;

    public class RequestUrlTask
    {
        private readonly ITypedMessageQueue queue;

        private readonly IDataStore store;
        public RequestUrlTask(ITypedMessageQueue queue, IDataStoreFactory factory, string connectionName)
        {
            this.queue = queue;
            this.store = factory.Create(connectionName);
        }
        
        public Task ExecuteAsync()
        {
            return this.queue.PollProcessUpdate<Messages.MonitorRequestMessage>((x) => this.Process(x));
        }

        private Task Process(MonitorRequestMessage x)
        {
            //            try
            //            {
            return this.RunRequest(x);
            //            }
            //            catch (Exception ex)
            //            {
            //                this.Log(x, ex);
            //            }
        }

        private async Task RunRequest(MonitorRequestMessage message)
        {
            var request = WebRequest.CreateHttp(message.Url);
            request.Accept = "application/json";
            request.ContentType = "application/json";
            
            request.Method = message.Method;
            
            var serializer = new Base2art.Serialization.NewtonsoftSerializer();
            
            if (message.Body != null)
            {
                var body = Encoding.Default.GetBytes(serializer.Serialize(message.Body));
                request.ContentLength = body.Length;
                using (var stream = await request.GetRequestStreamAsync())
                {
                    stream.Write(body, 0, body.Length);
                }
            }
            
            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    using(var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            await this.LogResponse(message, await reader.ReadToEndAsync());
                        }
                    }
                }
            }
        }

        private Task LogResponse(MonitorRequestMessage message, string content)
        {
            return this.store.Insert<monitor_execution_v1>()
                .Record(rs => rs.Field(x => x.monitor_id, message.Id)
                        .Field(x => x.result, content)
                        .Field(x => x.when, DateTime.UtcNow))
                .Execute();
        }
        
        private async void Log(MonitorRequestMessage message, Exception ex)
        {
            var content = string.Format(
                "{1}{0}{0}{2}{0}{0}{3}",
                Environment.NewLine,
                ex.GetType().AssemblyQualifiedName,
                ex.Message,
                ex.StackTrace);
            
            await this.LogResponse(message, content);
        }
    }
}
