﻿namespace Examples.SiteMonitor.Data
{
    using System;

    public interface monitor_execution_v1
    {
        Guid monitor_id { get; }
        
        string result { get; }
        
        DateTime when { get; }
    }
}
