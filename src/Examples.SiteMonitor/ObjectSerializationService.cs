﻿namespace Examples.SiteMonitor
{
    using Base2art.Serialization;
    
    public class ObjectSerializationService : Base2art.GatedQueue.IObjectSerializationService
    {
        private readonly ISerializer serializer;

        public ObjectSerializationService()
        {
            this.serializer = new NewtonsoftSerializer();
        }
        
        public string SerializeObject(object data)
        {
            return this.serializer.Serialize(data);
        }
        
        public T DeserializeObject<T>(string data)
        {
            return this.serializer.Deserialize<T>(data);
        }
    }
}
