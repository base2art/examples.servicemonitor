﻿namespace Examples.SiteMonitor.Public.Resources
{
    using System;
    using System.Collections.Generic;

    public class Monitor
    {
        public Guid Id { get; set; }
        
        public string Url { get; set; }

        public string Method { get; set; }
        
        public Dictionary<string, object> Body { get; set; }
        
        public bool IsActive { get; set; }
        
        public TimeSpan? Interval { get; set; }
    }
}
