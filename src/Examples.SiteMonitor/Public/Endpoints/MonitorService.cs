﻿namespace Examples.SiteMonitor.Public.Endpoints
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.Tasks;
    using Base2art.Validation;
    using Examples.SiteMonitor.Data;
    using Resources;
    
    public class MonitorService
    {
        private readonly IDataStore store;

        public MonitorService(IDataStoreFactory store, string connectionName)
        {
            this.store = store.Create(connectionName);
        }
        
        public Task<Monitor[]> GetMonitors()
        {
            return this.store.Select<monitor_v1>()
                .Execute()
                .Then()
                .Select(this.Convert)
                .Then()
                .ToArray();
        }
        
        public Task<Monitor> AddMonitor(Monitor monitor)
        {
            monitor.Validate().IsNotNull();
            
            if (monitor.Id == Guid.Empty)
            {
                monitor.Id = Guid.NewGuid();
            }
            
            monitor.Url.Validate().IsNotNullEmptyOrWhiteSpace();
            monitor.Method.Validate().IsNotNullEmptyOrWhiteSpace();
            
            return this.store.Insert<monitor_v1>()
                .Record(rs => rs.Field(x => x.id, monitor.Id)
                        .Field(x => x.body, monitor.Body)
                        .Field(x => x.method, monitor.Method)
                        .Field(x => x.interval, monitor.Interval.GetValueOrDefault(TimeSpan.FromMinutes(1)))
                        .Field(x => x.url, monitor.Url)
                        .Field(x => x.is_active, true))
                .Execute()
                .Then()
                .Return(monitor);
        }
        
        
        private Monitor Convert(monitor_v1 monitor)
        {
            return new Monitor {
                Body = monitor.body,
                Id = monitor.id,
                IsActive = monitor.is_active,
                Url = monitor.url,
                Interval = monitor.interval,
                Method = monitor.method
            };
        }
    }
}
