﻿
namespace Examples.SiteMonitor.Public.Tasks
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.Upgrade;
    using Examples.SiteMonitor.Data;
    
    public class UpgradeDatabase
    {
        private readonly IDbms dbms;

        private readonly bool canIfDrop;

        private readonly INamedConnectionDataDefinitionUpgrade[] upgrades;

        private readonly IDbmsFactory dbmsFactory;
        
        public UpgradeDatabase(IDbmsFactory dbms, string connectionName, bool canIfDrop, INamedConnectionDataDefinitionUpgrade[] upgrades)
        {
            this.upgrades = upgrades;
            this.canIfDrop = canIfDrop;
            this.dbmsFactory = dbms;
            this.dbms = dbms.Create(connectionName);
        }
        
        public async Task ExecuteAsync()
        {
            await this.CreateTable(() => this.dbms.CreateTable<monitor_v1>());
            await this.CreateTable(() => this.dbms.CreateTable<monitor_execution_v1>());
            
            foreach (var item in this.upgrades)
            {
                await item.Execute(this.dbmsFactory, new DataDefinitionUpgradeProperties {
                                 DbmsCanDropTables = this.canIfDrop,
                                 IsProduction = true
                             });
            }
        }

        private async Task CreateTable<T>(Func<ITableCreator<T>> par)
        {
            try
            {
                if (canIfDrop)
                {
                    await par().IfNotExists().Execute();
                }
                else
                {
                    await par().Execute();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return;
            }
        }
    }
}
