﻿

$msbuild = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"

if (-Not (Test-Path $msbuild)) {
  $msbuild = "C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
}


if (Test-Path ".deploy") {
  echo "deleting .deploy"
  rm -Recurse -Force ".deploy"
}



echo "Cleaning..."
& $msbuild /t:Clean /verbosity:quiet /p:Configuration=Debug /m /nologo
& $msbuild /t:Clean /verbosity:quiet /p:Configuration=Release /m /nologo



echo "Building..."
& $msbuild /t:Build /verbosity:quiet /p:Configuration=Debug /m /p:DebugType=PdbOnly /nologo

echo "RE-Building..."
& $msbuild /t:Build /verbosity:quiet /p:Configuration=Debug /m /p:DebugType=PdbOnly /nologo



"building a package..."
.\packages\Base2art.WebApiRunner.Deployer.0.0.1.1\tools\deployer\Base2art.WebApiRunner.Deployer.exe .\environments.json

exit 0;
